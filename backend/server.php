<?php
header("Content-Type:application/json");
header("Access-Control-Allow-Origin: *");

class MyDB extends SQLite3{
  function __construct(){
     $this->open('baza.db',SQLITE3_OPEN_READONLY);
  }
}

$db = new MyDB();

$results = $db->query('select * from Wpisy order by random() limit 1');

$data = $results->fetchArray();

$img = file_get_contents( 'data/' . $data[1]);
$imgb64 = base64_encode($img);

response($imgb64, $data[2]);

$db->close();

function response($file, $amount, $response_code = "200", $response_desc = "OK"){
	$response['file'] = $file;
	$response['amount'] = $amount;

	$response['response_code'] = $response_code;
	$response['response_desc'] = $response_desc;
	
	echo json_encode($response);
}
?>
