var fullstar = new Image();
var emptystar = new Image();

var pointerX = -1;
var pointerY = -1;

var amount;

var imgdata;
var canvas;
var ctx ;
var scoreobj;

function init() {

    scoreobj = document.getElementById("score");

    document.getElementById("canvas").addEventListener("mousemove", (e) => {

        var X = pointerX;
        var A = 2;
        var B = 498;
        var C = 0.0;
        var D = 5.0;

        var res = (X-A)/(B-A) * (D-C) + C;

        scoreobj.innerHTML = res.toFixed(2);

        pointerX = e.clientX - e.target.getBoundingClientRect().left;
        pointerY = e.clientY - e.target.getBoundingClientRect().top;

    })

    emptystar.src = 'starsGray.png';
    fullstar.src = 'starsYellow.png';

    canvas = document.getElementById('canvas');
    if (canvas.getContext) {
        ctx = canvas.getContext('2d');
        window.requestAnimationFrame(draw);
    }

    reload();
}

function reload() {
   
    imgdata =  document.getElementById("cover");

    requestdata();
    amount = jsondata.amount;

    var datastring = "data:image/png;base64, " + jsondata.file ;
    imgdata.src = datastring;

    scoreobj.innerHTML = "czekam na wybór";
}

setInterval(pointerCheck, 1);
function pointerCheck() {
      console.log('Cursor at: '+pointerX+', '+pointerY);
}

function draw() {

        ctx.globalCompositeOperation = 'destination-over';
        ctx.clearRect(0, 0, 500, 150); // clear canvas

        ctx.drawImage(emptystar,0,0);
        ctx.clearRect(0, 0, pointerX, 150); // clear star
        ctx.drawImage(fullstar,0,0,500,150);

        ctx.beginPath();
        ctx.moveTo(pointerX, 0);
        ctx.lineTo(pointerX, 150);
        ctx.stroke(); 
       
        window.requestAnimationFrame(draw);
}

function clickcanvas(event) {
    if (event.button == 0) {
        var usrerror = Math.abs( scoreobj.innerHTML - amount);
       alert("Twoja ocena : " +  scoreobj.innerHTML + "\nOcena społeczności : " + amount + "\nPomyłka o " + usrerror.toFixed(2) + "\nNaciśnij 'OK' aby grać dalej" );
       scoreobj.innerHTML = "Loading..."
       reload();
    }
}
